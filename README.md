# LeasePlanAssignment

Lease Plan BE QA AUTO TEST ASSIGNMENT.


**Installation**

1. Clone the project via the command below.

   `git clone https://gitlab.com/thenutester/Leaseplan-assignment.git`

   In case you get an error "`command not found: git`", please install git to your machine. 
   
   Kindly check resources below.

   [Download GIT](https://git-scm.com/downloads)

   [Digital Ocean blog about GIT](https://www.digitalocean.com/community/tutorials/how-to-contribute-to-open-source-getting-started-with-git)
    
   [Atlassian GIT tutorial](https://www.atlassian.com/git/tutorials/install-git) 


2. Once you have cloned the repository, open it via your IDE(Maven supporting). I am using IntelliJ IDEA.

3. Enter the command below to install dependencies and start testing your project.(It is running the test file because of the maven hierarchy.)

   `mvn clean install`

4. Lastly, you should configure your Maven runner to run test project.

   - Go to "Run" on your toolbar.
   - Click "Edit Configurations".
   - Click "+" icon on top left.
   - Click "Maven" on left hand side of window.
   - Assign a name to your configuration or leave it as default.(In my case it is artifactId in pom.xml file)
   - Paste the line below to [Command Line] input field.

      `prettier:write clean verify serenity:aggregate`
      
   - Click "Apply" and click "OK" buttons.


**Run Current Tests**

1. Simply click "Run" on toolbar and click "Run" button on the list.(Check that run is executed via added configuration.)


**Contribute Your Scenarios to Feature File**

1. Select your desired steps from the path below.
    > leaseplanassignment/src/test/java/starter/stepdefinitions/SearchStepDefinitions.java

2. Go to file below.
   > leaseplanassignment/src/test/resources/features/search/search_product.feature

3. Name your scenario with the definers below.
 
   - In case you have a data table to provide for your scenario, name it as > Scenario Outline
   - In case you **don't** need to provide a data table, name it as > Scenario

     `Scenario Outline: As a API user, when I call valid product endpoints, I should get corresponding values`


4. Add your test steps with the corresponding step definer(Given, When, Then).  

   Plus, replace {} placeholders with the parameter names which you define in data table. 

   `When I call "<product>" endpoint`
   
   `Then I should see the results displayed for "<product>"`
    Note : The given products' apple,mango,tofu,water is not available, so the api response will not be as expected. But still covered this scenario in positive case but when execute this scenario will be failied

5. In case you need to provide a data table, write it below to your scenario as given the example below.(Including "Examples" line)

   Examples:
      | product |
      | apple   | 
      | mango   | 


6. If you would like to tag your scenario with a tag, simply write it just above the "Scenario" or "Scenario Outline" lines.

@POSITIVE
  Scenario Outline: As a API user, when I call valid product endpoints, I should get corresponding values

7. If you would like to add more step definitions, please check the documentation of Serenity BDD.

   [Serenity BDD Documentation](https://serenity-bdd.github.io/theserenitybook/latest/index.html)


**Check Your Test Results**

1. Once your run is completed, gitlab CI automatically creates the artifact as downloadable item next to `test` job.

   **Note:** If you do not provide `serenity:aggregate` in your Maven configuration, your consolidated report will not be created.

After you download the artifact, kindly go to the path and open index.html file in your unzipped folder.

   `./target/site/serenity/index.html`


   If you are running your tests on IntelliJ IDEA, directly running the "link" below in your browser might help.

   http://localhost:63342/LeasePlanAssignment/target/site/serenity/index.html

 
# Changes

1. I have changed the naming of **post_product.feature** file to **search_product.feature**

2. I have changed the naming of **CarsAPI** to **SearchAPI** as per the objective of API.

3. I have changed the subject pronouns from 3rd singular masculine(He) to 1st singular(I). --> General approach

4. I have coded matcher step function for the title values in the response. --> Search API.java:17-21

5. I have changed the argument naming of the search api call function from **arg0** to **product**. --> SearchAPI.java:12,13

6. I have removed out redundant **gradle** files in project directory.

7. I have removed out redundant configurations and dependencies in **pom.xml** file.(e.g. distribution management)

8. I have added **prettier** dependency in **pom.xml** and its configuration to prettify the files.

9. I have added **Hooks** file to initialise baseUrl before starting to run scenarios.

10. I have added gitlab-ci configuration file to run the tests in pipeline and have the results as artifacts.

11. I have added tags to create aggregation and to differentiate scenarios from each other.

12. I have added **testFailureIgnore** as `true` and **skipTests** as `false` in **maven-failsafe-plugin** configurations.

13. I have populated README.md file with some useful information.



