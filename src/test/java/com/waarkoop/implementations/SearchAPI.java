package com.waarkoop.implementations;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

import io.cucumber.java.en.Then;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.hamcrest.Matchers;

public class SearchAPI {

  @Step("User call {string} endpoint")
  public void UserCallEndpoint(String product) {
    SerenityRest.get(product);
  }

  @Step("User see the results displayed for {string}")
  public void UserSeeTheResultsDisplayedFor(String product) {
        restAssuredThat(response -> response.statusCode(200));
    restAssuredThat(
      response -> response.body("title", Matchers.hasItem(Matchers.is(product)))
    );
  }

  @Step("User see the results displayed for not found")
  public void userShouldGetTheResponse() {
    restAssuredThat(response -> response.statusCode(404));
    String message = "Not Found";
    restAssuredThat(response -> response.body("detail", Matchers.is(message)));

  }

  @Step("User call the get search test endpoint")
  public void userCallTheGetSearchTestEndpoint() {
        SerenityRest.get();
  }

  @Step("User should get unauthorized error in search result")
  public void userShouldGetUnauthorizedErrorInSearchResult() {
    restAssuredThat(response -> response.statusCode(401));
    String message = "Not authenticated";
    restAssuredThat(response -> response.body("detail", Matchers.is(message)));
  }

  @Step("User should get the response code {int}")
  public void userShouldGetTheResponseCode(int responseCode) {
    restAssuredThat(response -> response.statusCode((responseCode)));
  }

}
