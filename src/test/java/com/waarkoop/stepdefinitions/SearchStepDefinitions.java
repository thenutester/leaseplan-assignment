package com.waarkoop.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import com.waarkoop.implementations.SearchAPI;

public class SearchStepDefinitions {
  @Steps
  public SearchAPI searchAPI;

  @When("User call {string} endpoint")
  public void UserCallEndpoint(String product) {
    searchAPI.UserCallEndpoint(product);
  }

  @Then("User should see the results displayed for {string}")
  public void UserShouldSeeTheResultDisplayedFor(String product) {
    searchAPI.UserSeeTheResultsDisplayedFor(product);
  }


  @Then("User should get the response has not found")
  public void userShouldGetTheResponse() {
    searchAPI.userShouldGetTheResponse();
  }

  @When("User call the get search test endpoint")
  public void userCallTheGetSearchTestEndpoint() {
    searchAPI.userCallTheGetSearchTestEndpoint();
  }

  @Then("User should get unauthorized error in search result")
  public void userShouldGetUnauthorizedErrorInSearchResult() {
    searchAPI.userShouldGetUnauthorizedErrorInSearchResult();
  }

  @Then("User should get the response code {string}")
  public void userShouldGetTheResponseCode(String responseCode) {
    searchAPI.userShouldGetTheResponseCode(Integer.parseInt((responseCode)));
  }
}
