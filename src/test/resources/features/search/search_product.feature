Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water" (but currently these products are not available, so you can see the positive scenario will be failed)
### Prepare Positive and negative scenarios

  @POSITIVE
  Scenario Outline: As a API user, when I call valid product endpoints, I should get corresponding values
    When User call "<product>" endpoint
    Then User should see the results displayed for "<product>"
    Examples:
      | product |
      | mango   |
      | apple   |
      | tofu    |

  @NEGATIVE1
  Scenario Outline: As a API user, when I call invalid endpoints, I should get not found message in the response
    When User call "<product>" endpoint
    Then User should get the response has not found
    Examples:
      | product |
      | car     |

  @NEGATIVE2
  Scenario: Search test without product, I should get unauthorized error message in the response
    When User call the get search test endpoint
    Then User should get the response code "401"
    Then User should get unauthorized error in search result
